# Ansible playbooks for OpenCell service provisioning

This an ansible playbook using ansible's docker modules to handle provisioning of OpenCell services.

## Requirements

 * To run ansible: python >=3.5 in **both server and client**
 * To user ansible's docker modules: docker python PyPI package.
 * `sudo` command in the server side.

We manage the requirements related to Python with the [Pyenv](https://github.com/pyenv/pyenv) tool. If you have it installed, run the next steps, if you don't, please look at [installation instructions](https://github.com/pyenv/pyenv-installer) and then run the next steps:

1. Install the Python 3.8.1 version

```commandline
$ pyenv install 3.8.1
```

2. Create the virtual environment called `opencell-provisioning`:

```commandline
$ pyenv virtualenv 3.8.1 opencell-provisioning
```

3. Install the Python dependencies:

```commandline
$ pyenv exec pip install -r requirements.txt
```

> Before installing the dependencies, please check that the virtual environment is active.

## Local environment

The tasks involved here make extensive use of docker but also modify the host system. In other playbooks we set up a linux container to insulate the environment from the host, but docker can't nest well inside lxc.

Therefore, we decided to use qemu with kvm to run the playbooks against it, i.e. the `opencell.local` host in the inventory.

Nevertheless, other virtualization tools can be used as long as the guest OS can be reached through IP (ansible can ssh into it) and have connectivity to the internet (ansible can download roles, data, packages...).

To do it our way you will need to
1. Complete the tutorial at [Virtualization with libvirt, qemu and kvm](https://github.com/coopdevs/handbook/wiki/Virtualization-with-libvirt,-qemu-and-kvm)
2. Make sure to append to `/etc/hosts` this line:  `192.168.122.72  opencell.local`, replacing the IP addr with the one of your VM.
3. In the following steps, limit ansible-playbook execution to your local env: `--limit opencell.local`

## Setup

 * Install ansible requirements using ansible-galaxy:

```commandline
$ pyenv exec ansible-galaxy install -r requirements.yml
```

 * Run sys_admins playbook:

At the first time:
```commandline
$ pyenv exec ansible-playbook playbooks/sys_admins.yml -l hosts -u root
```

The next times:
```commandline
$ pyenv exec ansible-playbook playbooks/sys_admins.yml -l hosts -u your-sys_admin-user
```

 * Run provision playbook:

```commandline
$ pyenv exec ansible-playbook playbooks/provision.yml -l hosts
```

 * Run deploy playbook:

```commandline
$ pyenv exec ansible-playbook playbooks/deploy.yml -l hosts
```

 * Configure `admin` and `superadmin` users

- Access to `<your-domain>/auth`
- Access to Admin Console
- Use the Keycloak user and Keycloak password to access.
- Access to Users, click on *View all users*
- Edit the `admin` and `superadmin` credentials.

## Giving access to the server

All app-related operations, such as starting/stopping docker are only possible by the `opencell_user`. Deployments are performed with this user however, you can't log into the server with it. If you need to perform any of these operations, you need to login as admin (see admin access below) and then change user with `sudo su - opencell`.
### Admin access

Admin access is controled with [playbooks/sys_admins.yml](https://github.com/coopdevs/trytond_provision/blob/master/playbooks/sys_admins.yml). For that playbook to take you into account your SSH key should be listed in `system_administrators`. Check out the playbook for more details.

## Vault Password

The vault password is in the Coopdevs Bitwarden

## Backups

This playbook uses coopdev's [backups role](https://github.com/coopdevs/backups_role). If you want to disable it, set in your inventory:
```yml
opencell_backups_enabled: false
```
or via other means such as a command-line "--extra-vars".

Otherwise, make sure that you are giving it all the data:
* where to store backups
* the authentication credentials
* the encryption secrets.

For a detailed list, see [`secrets.yml.example`](https://github.com/coopdevs/backups_role/blob/master/defaults/secrets.yml.example)

### Restore a backup

First of all, we need to find the backup version related with the date that you want to restore.
Look in the logs file `/var/log/cron.d/restic-stdout.log` and in the next table you can find the ID related with the date:

```
ID        Time                 Host                          Tags        Reasons           Paths
------------------------------------------------------------------------------------------------------------------------------------------
39c7e6b4  2019-11-05 03:45:47  stagingopencell.coopdevs.org              last snapshot     /home/opencell/input-files/opencell-version.txt
                                                                         daily snapshot    /opt/backup/.tmp/pg_dump_keycloak.sql
                                                                                           /opt/backup/.tmp/pg_dump_opencell.sql

0337ac29  2019-11-06 03:45:46  stagingopencell.coopdevs.org              last snapshot     /home/opencell/input-files/opencell-version.txt
                                                                         daily snapshot    /opt/backup/.tmp/pg_dump_keycloak.sql
                                                                                           /opt/backup/.tmp/pg_dump_opencell.sql

44cc5e0c  2019-11-07 03:45:49  stagingopencell.coopdevs.org              last snapshot     /home/opencell/input-files/opencell-version.txt
                                                                         daily snapshot    /opt/backup/.tmp/pg_dump_keycloak.sql
                                                                         weekly snapshot   /opt/backup/.tmp/pg_dump_opencell.sql
                                                                         monthly snapshot
------------------------------------------------------------------------------------------------------------------------------------------
```

Use `playbooks/restore_backup.yml` to download the selected backup to the host server.

```
$ ansible-playbook playbooks/restore_backup.yml -l my_host --ask-vault-pass -e "backups_role_restic_repo_version=44cc5e0c"
```

After running the playbook follow the steps listed in [Restore instance with backup](https://gitlab.com/coopdevs/opencell-provisioning/-/wikis/Restore-instance-with-backup).

### Generate backup manually

To generate a manually backup to reproduce the instance in other environment, we create a playbook to manage it.

First, use the playbook `generate_backup.yml` to generate a backup and fetch to the Ansible machine.
This playbook fetch the backup to a folder inside the playbooks folder: `playbooks/backups/`.

Run the playbook with:

```sh
$ ansible-playbook playbooks/generate_backup.yml -l my_host --ask-vault-pass -e "backup_path=<path>"
```

We use a volume to save the backup because it's too big. Ex: `backup_path=/mnt/Volume_HXXXX54/backup_folder`

OpenCell use the Jasper engine to generate the invoice PDFs and we need provide the templates to print the invoices with our image.
We use the [Som Connexió invoice template project](https://gitlab.com/coopdevs/somconnexio_invoice_jasper_template).


To manage this process we define the next vars:

```
invoice_template_project_name:        # Name of the project (in Gitlab) to deploy. Default: somconnexio_invoice_jasper_template
opencell_invoice_template_version:    # Version of the project to deploy
```

The process expect a release (tag) with the version declared to get the tar from the Gitlab repository like [v.0.0.0](https://gitlab.com/coopdevs/somconnexio_invoice_jasper_template/-/tags)

## Cleaning an instance

There's an alternative playbook to deploy opencell cleaning all data:

```commandline
$ pyenv exec ansible-playbook playbooks/clean_instance.yml -l hosts
```

## Upgrade an instance

There's a playbook to upgrade the OpenCell version.
We can check the new OpenCell releases in the Assembla Release Notes:

* OpenCell 7: https://opencell.assembla.com/spaces/meveo-enterprise/wiki/Opencell_7_Release_note
* OpenCell 8: https://opencell.assembla.com/spaces/meveo-enterprise/wiki/Opencell_8_Release_note

In this release notes we can find the migration script if is needed for any upgrade.

> You need a user in Assembla to access to these Release Notes

To run the upgrade, change the vars `opencell_version` and `migration_script_url` in the inventory and execute:

If no migration script is provided in the release notes, set empty the var `migration_script_url`.

```commandline
$ pyenv exec ansible-playbook playbooks/upgrade.yml -l hosts
```

## APM - [Glowroot](https://glowroot.org/)

OpenCell uses Glowroot APM.

We enable it in our instances and we have the user and password to access in Bitwarden.

You can access to Glowroot adding `/apm` to your URL.

* Staging: https://stagingopencell.coopdevs.org/apm/
* Production: https://opencell.coopdevs.org/apm/

The credentials can be found in BW with the name: `OpenCell Staging - Glowroot` or `OpenCell Production - Glowroot`.

## Monitoring

See [[https://gitlab.com/coopdevs/monitor-provisioning]]
