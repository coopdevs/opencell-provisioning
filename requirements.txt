ansible==2.8.5
ansible-lint==4.1.0
docker==3.7.0
yamllint==1.17.0
